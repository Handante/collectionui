//
//  MainViewController.swift
//  CollectionUI
//
//  Created by HahnDante on 07/05/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func touchSearchListButton(_ sender: Any) {
        self.navigationController?.pushViewController(ViewControllerBuilder.imgaeCollectionsViewController(), animated: true)
    }
    
}
