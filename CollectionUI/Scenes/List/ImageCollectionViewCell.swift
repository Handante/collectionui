//
//  ImageCollectionViewCell.swift
//  CollectionUI
//
//  Created by N4046 on 09/05/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
