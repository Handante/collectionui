//
//  ImageCollectionsViewController.swift
//  CollectionUI
//
//  Created by N4046 on 09/05/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import UIKit

class ImageCollectionsViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    static let cellId = "ImageCollectionsViewController"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureCollectionView()
        self.configureData()
    }
    
    func configureCollectionView() {
        collectionView.register(UINib(nibName: "ImageCollectionsViewController",
                                      bundle: nil), forCellWithReuseIdentifier: ImageCollectionsViewController.cellId)
    }
    
    func configureData() {
        SearchAdapter.requestImages("tags=landscape,portrait&tagmode=any") {
            
        }
    }
    
}

extension ImageCollectionsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionsViewController.cellId,
                                                      for: indexPath)
        
        return cell
    }
}

extension ImageCollectionsViewController: UICollectionViewDelegate {
    
}

extension ImageCollectionsViewController: UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView == self.collectionView {
            
        }
    }
}
