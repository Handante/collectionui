//
//  ViewControllerBuilder.swift
//  CollectionUI
//
//  Created by HahnDante on 09/05/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation
import UIKit

final class ViewControllerBuilder {
    
    static let storyboardName = "Main"
    static let imageCollectionsID = "ImageCollectionsViewController"
    
    static func imgaeCollectionsViewController() -> ImageCollectionsViewController {
        return UIStoryboard(name: ViewControllerBuilder.storyboardName, bundle: nil)
            .instantiateViewController(withIdentifier: ViewControllerBuilder.imageCollectionsID) as! ImageCollectionsViewController
    }
    
}
