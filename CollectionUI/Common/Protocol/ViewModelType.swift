//
//  ViewModelType.swift
//  MapUI
//
//  Created by HahnDante on 06/05/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

protocol ViewModelType {
    associatedtype Input
    associatedtype Output
    
    func transform(_ :Input) -> Output
}
