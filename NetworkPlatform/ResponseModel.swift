//
//  ResponseModel.swift
//  CollectionUI
//
//  Created by HahnDante on 16/04/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

public struct ResponseModel {
    public let request: URLRequest?
    public let reponse: HTTPURLResponse?
    public let data: Data?
    public let error: Error?
    public let isSucess: Bool
}
