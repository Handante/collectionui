//
//  SessionManager.swift
//  TabUI
//
//  Created by N4046 on 02/04/2019.
//  Copyright © 2019 Handante. All rights reserved.
//

import Foundation

public enum Result<Value> {
    case success(Value)
    case failure(Error)
    
    public var isSuccess: Bool {
        switch self {
        case .success:
            return true
        default:
            return false
        }
    }
    
    public var isFailure: Bool {
        return !isSuccess
    }
    
    public var value: Value? {
        switch self {
        case .success(let value):
            return value
        default:
            return nil
        }
    }
    
    public var error: Error? {
        switch self {
        case .success:
            return nil
        case .failure(let error):
            return error
        }
    }
}

struct DataResponse<Value> {
    public let request: URLRequest?
    public let response: HTTPURLResponse?
    public let data: Data?
    public let result: Result<Value>
}

class SessionManager {
    static let shared = SessionManager()
    let config: URLSessionConfiguration!
    let decoder = JSONDecoder()
    let defaultSession: URLSession?
    var request: URLRequest?
    var dataTask: URLSessionDataTask?
    
    init() {
        config = URLSessionConfiguration.default
        if #available(iOS 11.0, *) {
            config.waitsForConnectivity = true
        }
        defaultSession = URLSession(configuration: config)
    }
}

extension SessionManager {
    func task(_ urlRequest: URLRequest, success: @escaping (DataResponse<Data>) -> (), failure: @escaping(Error) -> ()) -> (Void) {
        self.dataTask?.cancel()
        self.dataTask = defaultSession?.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                failure(error)
            } else if let data = data, let response = response as? HTTPURLResponse {
                let dataResponse = DataResponse(request: urlRequest,
                                                response: response,
                                                data: data,
                                                result: Result.success(data))
                success(dataResponse)
            }
        }
        self.dataTask?.resume()
    }
    func task(_ urlRequest: URLRequest, success: @escaping (Data, HTTPURLResponse) -> (), failure: @escaping(Error) -> ()) -> (Void) {
        self.dataTask?.cancel()
        self.dataTask = defaultSession?.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                failure(error)
            } else if let data = data, let response = response as? HTTPURLResponse {
                success(data, response)
            }
        }
        self.dataTask?.resume()
    }
    
//    func urlSession<T: Decodable>(dataTask url: URL, success: @escaping((T?)->(Void)), failure: @escaping(Error)->(Void)) -> Void {
//        self.dataTask?.cancel()
//
//        let task = defaultSession?.dataTask(with: url) { (data, response, error) in
//            if let error = error {
//                failure(error)
//            } else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
//                success(data as? T)
//            }
//        }
//        task.resume()
//    }
    
}
