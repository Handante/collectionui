//
//  HttpAdapter.swift
//  CollectionUI
//
//  Created by HahnDante on 16/04/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation
import Kanna

public enum HttpMethod: String {
    case get    = "GET"
    case pose   = "POST"
    case delete = "DELETE"
    case put    = "PUT"
}

//    func urlSession<T: Decodable>(dataTask url: URL, success: @escaping((T?)->(Void)), failure: @escaping(Error)->(Void)) -> Void {
//        self.dataTask?.cancel()
//
//        let task = defaultSession?.dataTask(with: url) { (data, response, error) in
//            if let error = error {
//                failure(error)
//            } else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
//                success(data as? T)
//            }
//        }
//        task.resume()
//    }

//
//

class HttpAdapter {
    static func get<T: Decodable>(_ entity: String, param: [String: Any]?, failure: @escaping(Error) -> Void, success: @escaping(T?) -> Void) {
        var urlRequest = EndPoint.collection(term: "photos_public.gne?", entity: entity).request
        urlRequest.httpMethod = HttpMethod.get.rawValue
        urlRequest.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        send(urlRequest: urlRequest, failure: failure, success: success)
    }
    
    static func getHTML(_ entity: String, param: [String: Any]?, failure: @escaping(Error) -> Void, success: @escaping(String?) -> Void) {
        var urlRequest = EndPoint.collection(term: "photos_public.gne?", entity: entity).request
        urlRequest.setValue("text/html; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        send(urlRequest: urlRequest, failure: failure, success: success)
    }
    
    //    static func getUrl(_ path: String, queryString: String? = nil) -> URL {
    //        if let queryString = queryString {
    //            if let encodedQueryString = queryString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
    //                return URL(string: "\(HttpAdapter.domain)\(encodedQueryString)")!
    //            }
    //        }
    //        return URL(string: "\(HttpAdapter.domain)\(path)")!
    //    }

}

extension HttpAdapter {
    private static func send<T: Decodable>(urlRequest: URLRequest, failure: @escaping(Error) -> Void, success: @escaping(T?) -> Void) {
        self.processSend(urlRequest: urlRequest,
                         callback: { (responseModel: ResponseModel?) -> (Void) in
                let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601
                            do {
                                let dataModel = try decoder.decode(T.self, from: (responseModel?.data)!)
                                success(dataModel)
                                print(dataModel)
                            } catch let error {
                                print("\(HttpAdapter.self): send, \(error.localizedDescription)")
                                failure(error)
                            }
        }) { (error) -> (Void) in
            failure(error)
        }
    }
    
    private static func send(urlRequest: URLRequest, failure: @escaping(Error) -> Void, success: @escaping(String?) -> Void) {
        self.processSend(urlRequest: urlRequest) { (result) in
            print(result?.result.value)
        }
    }
    
    private static func processSend(urlRequest: URLRequest, callback: @escaping(DataResponse<Data>?) -> Void) {
        let sessionManager = SessionManager.shared
        sessionManager.task(urlRequest,
                            success: { (response: DataResponse) in
                       callback(response)
        }) { (error) in
            
        }
    }
    private static func processSend(urlRequest: URLRequest, callback: @escaping(ResponseModel?) -> (Void), failure: @escaping(Error) -> (Void) ) -> Void {
        let sessionManager = SessionManager.shared
        sessionManager.task(urlRequest,
                            success: { (data, response) in
                            let responseModel = ResponseModel(request: urlRequest,
                                                              reponse: response,
                                                              data: data,
                                                              error: nil,
                                                              isSucess: response.statusCode == 200 ? true: false)
                            callback(responseModel)
        }) { (error) in
            failure(error)
        }
    }
}
