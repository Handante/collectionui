//
//  ImageCacheService.swift
//  MapUI
//
//  Created by HahnDante on 07/05/2019.
//  Copyright © 2019 한상민. All rights reserved.
//

import Foundation

final class ImageCacheService {
    static let shared = ImageCacheService()
    
    private let imageDownloadQueue: OperationQueue = OperationQueue()
    private let fileManager = FileManager.default
    private var cacheDirectory: String {
        let diskPaths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
        let directory: String = diskPaths.first ?? ""
        return "\(directory)/cache"
    }
    
    var cache = NSCache<NSString, NSData>()
    
    init() {
        imageDownloadQueue.maxConcurrentOperationCount = 5
    }
    
    func getImage(_ url: String, completionHandler: @escaping((Data) -> ())) {
        let imageUrlHashValue = url.hashValue
        let directory = cacheDirectory
        let diskPath: String = "\(cacheDirectory)/\(imageUrlHashValue)"
        
        if let data = self.getCacheImageForKey(url) {
            completionHandler(data as Data)
        } else if fileManager.fileExists(atPath: diskPath) {
            do {
                if let data = try? Data(contentsOf: URL(fileURLWithPath: diskPath)) {
                    completionHandler(data)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        } else {
            self.downloadImage(url) { [weak self] (data) in
                let isExist = self?.fileManager.fileExists(atPath: directory)
                
                if isExist == false {
                    try? self?.fileManager.createDirectory(atPath: directory,
                                                           withIntermediateDirectories: true,
                                                           attributes: nil)
                }
                do {
                    try data.write(to: URL(fileURLWithPath: diskPath), options: .atomic)
                } catch let error {
                    print(error)
                }
                completionHandler(data)
            }
        }
    }
}
// MARK: Clear
extension ImageCacheService {
    public func clearDownloadQueue() {
        imageDownloadQueue.cancelAllOperations()
    }
    
    public func clearDiskCache() {
        let cacheDirectory = self.cacheDirectory
        try? fileManager.removeItem(atPath: cacheDirectory)
    }
    
    public func clearMemoryCache() {
        cache.removeAllObjects()
    }
}
// MARK: Caching
extension ImageCacheService {
    private func setCacheImageForKey(_ data: NSData, key: String) {
        self.cache.setObject(data, forKey: key as NSString)
    }
    
    private func getCacheImageForKey(_ key: String) -> NSData? {
        return self.cache.object(forKey: key as NSString)
    }
}
// MARK: Downloading
extension ImageCacheService {
    private func downloadImage(_ url: String, completionHandler: @escaping((Data)->())) {
        imageDownloadQueue.addOperation {
            let imageUrl = URL(string: url)
            let imageData = try? Data(contentsOf: imageUrl!)
            
            if let data = imageData {
                self.setCacheImageForKey(data as NSData,
                                         key: url)
                completionHandler(data)
            }
        }
    }
}
